/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sslsocket;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyStore;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 *
 * @author profe
 */
public class SSLClient {

    /**
     * @param args the command line arguments
     */
 public static void main(String args[]) throws Exception 
    {
       /* 
   System.setProperty("javax.net.ssl.keyStore", "selfsigned");
    System.setProperty("javax.net.ssl.keyStorePassword", "password");
        */
        try
        {
            
            SSLContext ctx = SSLClient.getSSLContext("../keystore.jks", "password");
            SSLServerSocketFactory sslFactory = ctx.getServerSocketFactory();
            
        //Mo 1 client socket den server voi so cong va dia chi xac dinh
        SSLSocketFactory factory=(SSLSocketFactory) SSLSocketFactory.getDefault();
        SSLSocket sslsocket=(SSLSocket) factory.createSocket("127.0.0.1",1234);

        //Tao luong nhan va gui du lieu len server
        DataOutputStream os=new DataOutputStream(sslsocket.getOutputStream());
        DataInputStream is=new DataInputStream(sslsocket.getInputStream());

        //Gui du lieu len server
        String str="helloworld";
        os.writeBytes(str);

        //Nhan du lieu da qua xu li tu server ve
        String responseStr;
        if((responseStr=is.readUTF())!=null)
        {
            System.out.println(responseStr);
        }

        os.close();
        is.close();
        sslsocket.close();
        }
        catch(UnknownHostException e)
        {
             System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
 
    private static SSLContext getSSLContext(String path, String password) throws Exception {
        SSLContext ctx = null;

        try {
            char[] passphrase = password.toCharArray();
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(new FileInputStream(path), passphrase);

            System.out.println("Certificats: " + keyStore.size());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(keyStore);
            ctx = SSLContext.getInstance("SSL");
            ctx.init(null, tmf.getTrustManagers(), null);

        } catch (IOException e) {
            System.out.print(e);
        }

        return ctx;
    } 
    
}
