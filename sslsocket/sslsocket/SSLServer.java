/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sslsocket;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyStore;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 *
 * @author profe
 */
public class SSLServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws Exception {
        System.out.println("Server......");
        
        
  /*      
   System.setProperty("javax.net.ssl.keyStore", "selfsigned");
    System.setProperty("javax.net.ssl.keyStorePassword", "password");
    */    
        try {

            SSLContext ctx = SSLServer.getSSLContext("../keystore.jks", "password");
 //           SSLContext ctx = SSLServer.getSSLContext("selfsigned", "password");
            SSLServerSocketFactory sslFactory = ctx.getServerSocketFactory();

            SSLServerSocket sslserversocket = (SSLServerSocket) sslFactory.createServerSocket(1234);
            //Tạo 1 đối tượng Socket từ serversocket để lắng nghe và chấp nhận kết nối từ client
            SSLSocket sslsocket = (SSLSocket) sslserversocket.accept();
            //Tao cac luong de nhan va gui du lieu cua client
            DataInputStream is = new DataInputStream(sslsocket.getInputStream());
            PrintStream os = new PrintStream(sslsocket.getOutputStream());
            while (true) //khi dang ket noi voi client
            {
                //Doc du lieu den
                String input = is.readUTF();
                String ketqua = input.toUpperCase();
                //Du lieu tra ve
                os.println(ketqua);
            }
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    private static SSLContext getSSLContext(String path, String password) throws Exception {
        SSLContext ctx = null;

        try {
            char[] passphrase = password.toCharArray();
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(new FileInputStream(path), passphrase);

            System.out.println("Certificats: " + keyStore.size());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(keyStore);
            ctx = SSLContext.getInstance("SSL");
            ctx.init(null, tmf.getTrustManagers(), null);

        } catch (IOException e) {
            System.out.print(e);
        }

        return ctx;
    }
}
