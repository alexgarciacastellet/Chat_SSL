/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sslmonday;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import sslsocket.SSLServer;

/**
 *
 * @author alex
 */
public class SSLMonday {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws KeyStoreException, IOException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        SSLMonday.func1();
    
}

    private static void func1() {
        
        try {
            SSLContext ctx = SSLMonday.getSSLContext("../keystore.jks", "password");
 //           SSLContext ctx = SSLServer.getSSLContext("selfsigned", "password");
            SSLServerSocketFactory sslFactory = ctx.getServerSocketFactory();

            SSLServerSocket sslserversocket = (SSLServerSocket) sslFactory.createServerSocket(1234);
           // ServerSocket ss = sslFactory.createServerSocket(1234);
            while (true) {
                Socket s = sslserversocket.accept();
                PrintStream out = new PrintStream(s.getOutputStream());
                out.println("Hi");
                out.close();
                s.close();        
            }   } catch (IOException ex) {
            Logger.getLogger(SSLMonday.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SSLMonday.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    protected static SSLContext getSSLContext(String path, String password) throws Exception {
        SSLContext ctx = null;

        try {
            char[] passphrase = password.toCharArray();
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(new FileInputStream(path), passphrase);

            System.out.println("Certificats: " + keyStore.size());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(keyStore);
            ctx = SSLContext.getInstance("SSL");
            ctx.init(null, tmf.getTrustManagers(), null);

        } catch (IOException e) {
            System.out.print(e);
        }

        return ctx;
    } 
    
}
